#!/bin/sh




for folder in 'Aa' 'Ab' 'Ac' 'Ba' 'Bb' 'Bc' 'Ca'
do
for case in 'nc1' 'nc2' 'nc3' 'tc1' 'tc2'
do
echo "data_transform/bags/$folder/$case""_label.bag"
rostopic echo /scan -b ~/data_transform/bags/$folder/$case""_label.bag -p > ~/data_transform/csvdata/$folder/lid_$case.csv
rostopic echo /cam2lid -b ~/data_transform/bags/$folder/$case""_label.bag -p > ~/data_transform/csvdata/$folder/cam_$case.csv
rostopic echo /key_label_pub -b ~/data_transform/bags/$folder/$case""_label.bag -p > ~/data_transform/csvdata/$folder/label_$case.csv
done
done

for folder in 'Cb' 'Cc'
do
for case in 'nc1' 'nc2' 'nc3'
do
echo "data_transform/bags/$folder/$case""_label.bag"
rostopic echo /scan -b ~/data_transform/bags/$folder/$case""_label.bag -p > ~/data_transform/csvdata/$folder/lid_$case.csv
rostopic echo /cam2lid -b ~/data_transform/bags/$folder/$case""_label.bag -p > ~/data_transform/csvdata/$folder/cam_$case.csv
rostopic echo /key_label_pub -b ~/data_transform/bags/$folder/$case""_label.bag -p > ~/data_transform/csvdata/$folder/label_$case.csv
done
done

