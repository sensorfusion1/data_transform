import pandas as pd
import numpy as np
import os
from prep_data import load_data


# load_data('Aa/lid_tc1', 'Aa/cam_tc1', 'Aa/label_tc1')

folders = ['Aa', 'Ab', 'Ac', 'Ba', 'Bb', 'Bc', 'Ca'] #Cb and Cc special
cases = ['nc1', 'nc2', 'nc3', 'tc1', 'tc2']

for folder in folders:
    for case in cases:
        load_data(folder+'/lid_'+case, folder+'/cam_'+case, folder+'/label_'+case)


foldersC = ['Cb', 'Cc']
casesC = ['nc1', 'nc2', 'nc3']

for folder in foldersC:
    for case in casesC:
        load_data(folder+'/lid_'+case, folder+'/cam_'+case, folder+'/label_'+case)