import numpy as np
import os



folders = ['Aa', 'Ab', 'Ac', 'Ba', 'Bb', 'Bc', 'Ca'] #Cb and Cc special
cases = ['nc1', 'nc2', 'nc3', 'tc1', 'tc2']

data_nc = np.empty((1,787))
error_lidar = np.empty((1,787))
error_camera = np.empty((1,787))

for folder in folders:
    for case in cases:
        label_detect = np.load(os.path.expanduser('~/network/npydata/'+folder+'/label_'+case+'.npy')) 
        cam = np.load(os.path.expanduser('~/network/npydata/'+folder+'/cam_'+case+'.npy'))
        lid = np.load(os.path.expanduser('~/network/npydata/'+folder+'/lid_'+case+'.npy'))
        cam_errors = np.where(label_detect[:,1] == 1)[0]
        lid_errors = np.where(label_detect[:,2] == 1)[0]
        no_errors = np.where(label_detect[:,0] == 1)[0]
        # print('case', case, label_detect.shape, cam.shape, lid.shape)
        # print('cam error',len(cam_errors), len(lid_errors))

        conc_set = np.concatenate((label_detect, cam, lid), axis=1)

        data_nc = np.concatenate((data_nc, conc_set[no_errors,:]), axis=0)
        error_lidar = np.concatenate((error_lidar, conc_set[lid_errors,:]), axis=0)
        error_camera = np.concatenate((error_camera, conc_set[cam_errors,:]), axis=0)
 

# Remove top empty row
data_nc = data_nc[1:,:]
error_camera = error_camera[1:,:]
error_lidar = error_lidar[1:,:]

foldersC = ['Cb', 'Cc']
casesC = ['nc1', 'nc2', 'nc3']

for folder in foldersC:
    for case in casesC:
        label_detect = np.load(os.path.expanduser('~/network/npydata/'+folder+'/label_'+case+'.npy')) 
        cam = np.load(os.path.expanduser('~/network/npydata/'+folder+'/cam_'+case+'.npy'))
        lid = np.load(os.path.expanduser('~/network/npydata/'+folder+'/lid_'+case+'.npy'))
        cam_errors = np.where(label_detect[:,1] == 1)[0]
        lid_errors = np.where(label_detect[:,2] == 1)[0]
        no_errors = np.where(label_detect[:,0] == 1)[0]
        # print('case', case, label_detect.shape, cam.shape, lid.shape)
        # print('cam error',len(cam_errors), len(lid_errors))

        conc_set = np.concatenate((label_detect, cam, lid), axis=1)

        data_nc = np.concatenate((data_nc, conc_set[no_errors,:]), axis=0)
        error_lidar = np.concatenate((error_lidar, conc_set[lid_errors,:]), axis=0)
        error_camera = np.concatenate((error_camera, conc_set[cam_errors,:]), axis=0)
 

print('Number of samples per cases', data_nc.shape, error_camera.shape, error_lidar.shape)

# train, val, test percentage
div = [0.75, 0.9]

nnc = data_nc.shape[0] 
ncam = error_camera.shape[0]
nlid = error_lidar.shape[0]

# print(nnc, ncam, nlid)

## Make sure even percentates of all classes in train, val and test
train_nc, val_nc, test_nc = np.split(data_nc, [int(nnc*div[0]), int(nnc*div[1])])
train_cam, val_cam, test_cam = np.split(error_camera, [int(ncam*div[0]), int(ncam*div[1])])
train_lid, val_lid, test_lid = np.split(error_lidar, [int(nlid*div[0]), int(nlid*div[1])])

X_train = np.concatenate((train_nc, train_cam, train_lid))
X_validate = np.concatenate((val_nc, val_cam, val_lid))
test = np.concatenate((test_nc, test_cam, test_lid))

print('train, val, test shapes',X_train.shape, X_validate.shape, test.shape)

np.save(os.path.expanduser('~/network/npydata/x_train.npy'), X_train) 
np.save(os.path.expanduser('~/network/npydata/x_val.npy'), X_validate) 
np.save(os.path.expanduser('~/network/npydata/test.npy'), test) 


## Create the labels for the autoencoder which has the nans removed
Y_train = X_train
Y_val = X_validate

for row in range(Y_train.shape[0]):
    ins = np.where(Y_train[row,:] > 20)     ## all indexes out of range
    for i in range(len(ins[0])):
        Y_train[row, ins[0][i]] = Y_train[row, ins[0][i]-1]


for row in range(Y_val.shape[0]):
    ins = np.where(Y_val[row,:] > 20)     ## all indexes out of range
    for i in range(len(ins[0])):
        Y_val[row, ins[0][i]] = Y_val[row, ins[0][i]-1]

########################################################
## Augment the data to increase dataset
# ########################################################
# Gaussian noise -0.01,0.01
print('Data pre augmentation', Y_train.shape, Y_val.shape)

y_train_noise = np.random.normal(-0.001, 0.001, size=Y_train.shape)
y_val_noise = np.random.normal(-0.001, 0.001, size=Y_val.shape)
print('Noise ', y_train_noise.shape, y_val_noise.shape)

# Dont alter the labels
y_train_noise[:,0:3] = 0
y_val_noise[:,0:3] = 0

# Add noise to camera and lidar data
y_train_noise = y_train_noise + Y_train
y_val_noise = y_val_noise + Y_val

# Gaussian noise 0,0.015
########################################################
y_train_noise1 = np.random.normal(0, 0.015, size=Y_train.shape)
y_val_noise1 = np.random.normal(0, 0.015, size=Y_val.shape)
print('Noise 1', y_train_noise1.shape, y_val_noise1.shape)

# Dont alter the labels
y_train_noise1[:,0:3] = 0
y_val_noise1[:,0:3] = 0

# Add noise to camera and lidar data
y_train_noise1 = y_train_noise1 + Y_train
y_val_noise1 = y_val_noise1 + Y_val

# Gaussian noise -0.001,0.002
########################################################
y_train_noise2 = np.random.normal(-0.001, 0.002, size=Y_train.shape)
y_val_noise2 = np.random.normal(-0.001, 0.002, size=Y_val.shape)
print('Noise 2', y_train_noise2.shape, y_val_noise2.shape)

# Dont alter the labels
y_train_noise2[:,0:3] = 0
y_val_noise2[:,0:3] = 0

# Add noise to camera and lidar data
y_train_noise2 = y_train_noise2 + Y_train
y_val_noise2 = y_val_noise2 + Y_val

# Double dataset with noise -0.001, 0.005 
########################################################
Y_new = np.concatenate((y_train_noise, y_train_noise1, y_train_noise2, Y_train), axis=0)
Y_val_new = np.concatenate((y_val_noise, y_val_noise1, y_val_noise2, Y_val), axis=0)

y_train_noise3 = np.random.normal(-0.005, 0.001, size=Y_new.shape)
y_val_noise3 = np.random.normal(-0.005, 0.001, size=Y_val_new.shape)
print('Noise 3', y_train_noise3.shape, y_val_noise3.shape)

# Dont alter the labels
y_train_noise3[:,0:3] = 0
y_val_noise3[:,0:3] = 0

# Add noise to camera and lidar data
y_train_noise3 = y_train_noise3 + Y_new
y_val_noise3 = y_val_noise3 + Y_val_new

Y_new = np.concatenate((y_train_noise3, Y_new), axis=0)
Y_val_new = np.concatenate((y_val_noise3, Y_val_new), axis=0)

# Expanded dataset
########################################################

print('Post augmentation',Y_new.shape, Y_val_new.shape)

print(Y_new[5:10,5:10])


np.save(os.path.expanduser('~/network/npydata/y_train.npy'), Y_new) 
np.save(os.path.expanduser('~/network/npydata/y_val.npy'), Y_val_new) 

