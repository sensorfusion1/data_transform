#!/usr/bin/env python3
import pandas as pd
import numpy as np
import os

###############################################################
# Load data
###############################################################
# Rightclick on file in tree to get relative or absolute path


def one_hot(arr, dim):
    onehot = np.zeros((arr.shape[0], dim))
    for ind in range(arr.shape[0]):
        onehot[ind, int(arr[ind]-1)] = 1
    return onehot


def load_data(lid_path, cam_path, lab_path):
    df_lidar = pd.read_csv(os.path.expanduser('~/data_transform/csvdata/'+lid_path+'.csv'), header=0)
    df_camera = pd.read_csv(os.path.expanduser('~/data_transform/csvdata/'+cam_path+'.csv'), header = 0)
    df_labels = pd.read_csv(os.path.expanduser('~/data_transform/csvdata/'+lab_path+'.csv'), header = 0)

    print('input data ', df_lidar.shape, df_camera.shape, df_labels.shape)

    
    # Sync the time series on lidar, (take previous camera frame)
    ####################################################
    lidar_new = pd.DataFrame(columns = df_lidar.columns)
    camera_new = pd.DataFrame(columns = df_camera.columns)
    time = 'field.header.stamp'

    for i in range(df_lidar[time].shape[0]):
        inds = np.where((df_camera[time] < df_lidar[time][i]))
        
        
        if inds[0].size != 0:
            ind_max = inds[0][-1] 
            lidar_new = lidar_new.append(df_lidar.iloc[i], ignore_index = True)
            camera_new = camera_new.append(df_camera.iloc[ind_max], ignore_index = True)

    
    # Create dense labels array
    ####################################################

    labels = np.zeros((lidar_new.shape[0],))

    for i in range(df_labels[time].shape[0]):
        ins = np.where(df_labels[time][i] <= lidar_new[time])
        labels[ins[0]-1] = df_labels['field.string_data'][i]

    labels[-1] = df_labels['field.string_data'][df_labels[time].shape[0]-1]


    print('Time aligned data', lidar_new.shape, camera_new.shape, labels.shape)

    # Remove samples without a label in the beginning
    ####################################################
    ins = np.where(camera_new[time] < df_labels[time][0])
    s = len(ins[0])

    lidar_al = pd.DataFrame(columns = df_lidar.columns)
    camera_al = pd.DataFrame(columns = df_camera.columns)

    lidar_al = lidar_al.append(lidar_new.iloc[s-1:], ignore_index = True)   
    camera_al = camera_al.append(camera_new.iloc[s-1:], ignore_index = True)    
    labels = labels[s-1:]

    print('Label aligned data', lidar_al.shape, camera_al.shape, labels.shape)


    # lidar_al.to_csv('~/network/npydata/Aa/lid_tc1.csv')
    # camera_al.to_csv('~/network/npydata/Aa/cam_tc1.csv')
    # np.savetxt(os.path.expanduser('~/network/npydata/Aa/label_tc1.csv'), labels, delimiter=",")


    
    # Pluck relevant fields 
    ####################################################    
    
    col_lidar = [col for col in lidar_al if col.startswith('field.ranges')]
    lidar_range = lidar_al[col_lidar] 
    lidar_range = np.nan_to_num(lidar_range) #### posinf

    col_camera = [col for col in camera_al if col.startswith('field.ranges')]
    camera_range = camera_al[col_camera]
    camera_range = np.nan_to_num(camera_range) #### posinf

    label_detect = one_hot(labels, 3)

    print('field data', lidar_range.shape, camera_range.shape, label_detect.shape)



    # Save to file
    ####################################################  
    np.save(os.path.expanduser('~/data_transform/npydata/'+lid_path+'.npy'), lidar_range) 
    np.save(os.path.expanduser('~/data_transform/npydata/'+cam_path+'.npy'), camera_range) 
    np.save(os.path.expanduser('~/data_transform/npydata/'+lab_path+'.npy'), label_detect) 




# load_data('Aa/lid_tc1', 'Aa/cam_tc1', 'Aa/label_tc1')